public static int countDigit(int number){
		int count = 0;
		for(int i=number; i!=0; i=i/10) {
			count++;
		}
		return count;
	}
	public static boolean checkArmstrong(int number) {
		int temp = number;
		int count = countDigit(number);
		double sum = 0;
		for(; temp>0; temp = temp/10) {
			int rem = temp % 10;
			sum = sum + Math.pow(rem, count);
		}
		return (sum == number) ? true : false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(checkArmstrong(371));
		System.out.println(checkArmstrong(153));
		System.out.println(checkArmstrong(820));
		System.out.println(checkArmstrong(1634));
		System.out.println(checkArmstrong(8208));
		System.out.println(checkArmstrong(8000));
	}
public static void main(String[] args){
	System.out.println("Hello world");
}

